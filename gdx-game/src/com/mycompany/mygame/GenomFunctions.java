package com.mycompany.mygame;
import java.time.*;
import java.util.ArrayList;
//import java.util.concurrent.cop//
import com.badlogic.gdx.graphics.*;

public class GenomFunctions
{
	public void KillCell(Cell cell, ArrayList<Cell> list)
	{
		list.remove(cell);
		cell.Die();
	}
	
	public void step(Pane[][] field, Cell cell, genNames gen, ArrayList<Cell> list,ArrayList<Cell> newCells )
	{
		switch(gen)
		{
			case moveRight:{
				moveright(field, cell);
				break;
			}
			case moveLeft:{
				moveleft(field, cell);
					break;
			}
			case moveUp:{
				moveup(field, cell);
					break;
			}
			case moveDown:{
				movedown(field, cell);
					break;
			}
			case wait:{
				wait(field, cell);
				break;
			}
			case clone:
			{
				clone(field, cell, list, newCells);
				break;
			}
			case suicide:
			{
				suicide(field, cell, list);
				break;
			}
			case attackUp:
			{
				attackUp(field, cell, list);
				break;
			}
			case attackRight:
			{
				attackRight(field, cell, list);
				break;
			}
			case attackLeft:
			{
				attackLeft(field, cell, list);
				break;
			}
			case attackDown:
			{
				attackDown(field, cell, list);
				break;
			}
		}
		if (cell.energy <= 0)
		{
			KillCell(cell,list);
		}
		if (cell.energy >= Cell.energyToClone)
		{
			clone(field,cell,list, newCells);
		}
	}
	
	public void moveright(Pane[][] field, Cell cell)
	{
		int x = cell.getX();
		if (x >= Genom.size - 1) return;
		int y = cell.getY();
		synchronized(field[x+1][y])
		{
			try
			{wait();}
			catch(Exception e){}
		if (field[x+1][y].type == 0)//если справа пусто
		{
			//field[x+1][y] = new Cell(cell);
			//field[x][y] = new Pane(x,y);
			Pane t = field[x+1][y];
			field[x+1][y] = cell;
			field[x][y] = t;
			cell.x = x+1;
			
		}
			//notify();
		}
		cell.energy-=Cell.energyToMove;
	}
	
	public void moveleft(Pane[][] field, Cell cell)
	{
		int x = cell.getX();
		if (x == 0)
			return;
		int y = cell.getY();
		synchronized(field[x-1][y])
		{
			try
			{wait();}
			catch(Exception e){}
		if (field[x-1][y].type == 0)//если справа пусто
		{
			//field[x-1][y] = new Cell(cell);
			//field[x][y] = new Pane(x,y);
			Pane t = field[x-1][y];
			field[x-1][y] = cell;
			field[x][y] = t;
			cell.x = x-1;
		}
		//notify();
		cell.energy-=Cell.energyToMove;
		}
	}
	
	public void moveup(Pane[][] field, Cell cell)
	{
		int x = cell.getX();
		int y = cell.getY();
		if (y >= Genom.size - 1) return;
		synchronized(field[x][y+1])
		{
			try
			{wait();}
			catch(Exception e){}
		if (field[x][y+1].type == 0)//если справа пусто
		{
			//field[x][y+1] = new Cell(cell);
			//field[x][y] = new Pane(x,y);
			Pane t = field[x][y+1];
			field[x][y+1] = cell;
			field[x][y] = t;
			cell.y = y+1;
			//notify();
		}
		cell.energy-=Cell.energyToMove;
		}
	}
	
	
	public void movedown(Pane[][] field, Cell cell)
	{
		int x = cell.getX();
		int y = cell.getY();
		if (y == 0) return;
		synchronized(field[x][y-1])
		{
			try
			{wait();}
			catch(Exception e){}
		if (field[x][y-1].type == 0)//если справа пусто
		{
			//field[x][y-1] = new Cell(cell);
			//field[x][y] = new Pane(x,y);
			Pane t = field[x][y-1];
			field[x][y-1] = cell;
			field[x][y] = t;
			cell.y = y-1;
		}
		//notify();
		}
		cell.energy-=Cell.energyToMove;
	}
	
	public void wait(Pane[][] field, Cell cell)
	{
		cell.energy+=Cell.energyPerWait;
	}
	
	public void clone(Pane[][] field, Cell cell, ArrayList<Cell> list, ArrayList<Cell> newCells)
	{
		int child = Gen.r.nextInt(Cell.maxChild);
		if (child == 0)
			return;
		for (int i = cell.x-1; i<= cell.x +1;i++)
		{
			for (int j = cell.y-1; j<= cell.y +1;j++)
			{
				if (i != cell.x || j != cell.y)
				{
					if (child <= 0)
						return;
					if (i< 0 || j< 0 || i>=Genom.size || j>=Genom.size)
						continue;
					synchronized(field[i][j])
					{
						try
						{wait();}
						catch(Exception e){}
						if (field[i][j].type == 0)
						{
						if (Gen.r.nextInt(100) < Cell.childChince)
						{
							
						//	Cell c = new Cell(cell,true);
							Cell c = newCell(cell,i,j);
							//Cell c = new Cell(cell);
							//c.x = i;
							//c.y = j;
							c.energy = cell.energy/child;
							Genom.PaneContainer.add(field[i][j]);
							field[i][j] = c;
							
							newCells.add(c);
							child--;
						}
					}
					//notify();
					}
				}
			}
		}
		cell.energy = cell.energy/4;
		
	}
	
	public void suicide(Pane[][] field, Cell cell, ArrayList<Cell> list)
	{
		//list.remove(cell);
	//	field[cell.x][cell.y]= new Dead(cell.x,cell.y);
		if (Gen.r.nextInt(100) < Cell.suicideChince)
			KillCell(cell,list);
		else cell.energy+=Cell.energyPerWait;
	}
	
	public void attackUp(Pane[][] field, Cell cell, ArrayList<Cell> list)
	{
		/*int x = cell.getX();
		int y = cell.getY();
		
		if (y >= Genom.size - 1) return;
		
		if (field[x][y+1].type == 2 && field[x][y+1].team != cell.team)//клетка
		{ 
			Cell c2 = (Cell)field[x][y+1];
			int at = Gen.r.nextInt(cell.maxAttack);
			cell.energy -= at;
			c2.energy -= at*Cell.attackCoefficient;
			if (c2.energy <= 0)
				KillCell(c2,list);
		
		}else
		
		if (field[x][y+1].type == 3)//мертвяк
		{
			Cell c2 = (Cell)field[x][y+1];
			int at = Gen.r.nextInt(cell.maxAttack);
			cell.energy += at*Cell.attackCoefficient;
			c2.energy -= at;
			if (c2.energy <= 0)
			{
				Genom.CellContainer.add(c2);
				field[x][y+1] = Genom.newPane(x, y+1);
			}
			
		}*/
	}
	public void attackDown(Pane[][] field, Cell cell, ArrayList<Cell> list)
	{
		/*int x = cell.getX();
		int y = cell.getY();

		if (y <= 0) return;

		if (field[x][y-1].type == 2 && field[x][y-1].team != cell.team)//клетка
		{
			Cell c2 = (Cell)field[x][y-1];
			int at = Gen.r.nextInt(cell.maxAttack);
			cell.energy -= at;
			c2.energy -= at*Cell.attackCoefficient;
			if (c2.energy <= 0)
				KillCell(c2,list);
		
		}else

		if (field[x][y-1].type == 3)//мертвяк
		{
			Cell c2 = (Cell)field[x][y-1];
			int at = Gen.r.nextInt(cell.maxAttack);
			cell.energy += at*Cell.attackCoefficient;
			c2.energy -= at;
			if (c2.energy <= 0)
			{
				Genom.CellContainer.add(c2);
				field[x][y-1] = Genom.newPane(x,y-1);	
			}
			
		}*/
	}
	public void attackLeft(Pane[][] field, Cell cell, ArrayList<Cell> list)
	{/*
		int x = cell.getX();
		int y = cell.getY();

		if (x <= 0) return;

		if (field[x-1][y].type == 2 && field[x-1][y].team != cell.team)//клетка
		{
			Cell c2 = (Cell)field[x-1][y];
			int at = Gen.r.nextInt(cell.maxAttack);
			cell.energy -= at;
			c2.energy -= at*Cell.attackCoefficient;
			if (c2.energy <= 0)
				KillCell(c2,list);
		}else

		if (field[x-1][y].type == 3)//мертвяк
		{
			Cell c2 = (Cell)field[x-1][y];
			int at = Gen.r.nextInt(cell.maxAttack);
			cell.energy += at*Cell.attackCoefficient;
			c2.energy -= at;
			if (c2.energy <= 0)
			{
				Genom.CellContainer.add(c2);
				field[x-1][y] = Genom.newPane(x-1,y);
			}
			
		}*/
	}
	public void attackRight(Pane[][] field, Cell cell, ArrayList<Cell> list)
	{
		/*int x = cell.getX();
		int y = cell.getY();

		if (x >= Genom.size-1) return;

		if (field[x+1][y].type == 2 && field[x+1][y].team != cell.team)//клетка
		{
			Cell c2 = (Cell)field[x+1][y];
			int at = Gen.r.nextInt(cell.maxAttack);
			cell.energy -= at;
			c2.energy -= at*Cell.attackCoefficient;
			if (c2.energy <= 0)
				KillCell(c2,list);
			
		}else

		if (field[x+1][y].type == 3)//мертвяк
		{
			Cell c2 = (Cell)field[x+1][y];
			int at = Gen.r.nextInt(cell.maxAttack);
			cell.energy += at*Cell.attackCoefficient;
			c2.energy -= at;
			if (c2.energy <= 0)
			{
				Genom.CellContainer.add(c2);
				field[x+1][y] = Genom.newPane(x+1,y);
			}
			
		}*/
	}
	public  Cell newCell(Cell c, int x, int y)
	{
		Cell t;
		synchronized(Genom.CellContainer)
		{
			try{wait();}catch(Exception e){}
			t = Genom.CellContainer.get(0);
			Genom.CellContainer.remove(0);
		}
		t.copy(c,x,y);
		return t;
	}

	public  Pane newPane(int x, int y)
	{
		Pane t;
		synchronized(Genom.PaneContainer)
		{
			try{wait();}catch(Exception e){}
			t = Genom.PaneContainer.get(0);
			Genom.PaneContainer.remove(0);
		}
		t.x = x;
		t.y = y;
		return t;
	}
}
