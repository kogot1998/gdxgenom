package com.mycompany.mygame;
import java.util.*;
import java.util.function.*;

enum genNames
{
	moveRight,
	moveLeft,
	moveUp,
	moveDown,
	wait,
	clone,
	suicide,
	attackUp,
	attackLeft,
	attackRight,
	attackDown;
}

public class Gen
{
	static int genChance = 95;//шанс в процентах что ген останеься
	int iterator;
	ArrayList<genNames> list;
	static Random r = new Random(System.currentTimeMillis());
	
	public Gen()
	{
		//r = new Random(System.currentTimeMillis());
		list = new ArrayList<genNames>(Cell.genomSize);
		iterator = 0;
		fillWait();
	}
	
	public Gen(Gen g)
	{
		iterator = g.iterator;
		//r = new Random(System.currentTimeMillis());
		list = new ArrayList<genNames>(g.list);
	}
	
	public Gen(Gen g, boolean flag)// 
	{
		iterator = 0;
		//r = new Random(System.currentTimeMillis());
		list = new ArrayList<genNames>();
		if (flag == true)
		{
			for (int i = 0; i < g.list.size(); i++)
			{
				if (r.nextInt(100) > genChance)
				{
					//list.remove(i);
					int n = (r.nextInt(genNames.values().length));
					list.add(genNames.values()[n]);
				}else list.add(g.list.get(i));
			}
		}
	}
		
	public void copy(Gen g)
	{
		iterator = 0;
		list.clear();
		for (int i = 0; i < g.list.size(); i++)
		{
			if (r.nextInt(100) > genChance)
			{
				//list.remove(i);
				int n = (r.nextInt(genNames.values().length));
				list.add(genNames.values()[n]);
			}else list.add(g.list.get(i));
		}
	}
	
	public void fillAll(genNames n)
	{
		list.clear();
		for (int i = 0; i<Cell.genomSize; i++)
		{
			list.add(n);
		}
	}
	
	public void fillRand()
	{
		list.clear();
	
		for (int i = 0; i<Cell.genomSize; i++)
		{
			
			int n = (r.nextInt(genNames.values().length));
			list.add(genNames.values()[n]);
		}
	}
	
	public void fillWait()
	{
		list.clear();

		for (int i = 0; i<Cell.genomSize; i++)
		{
			list.add(genNames.wait);
		}
	}
	
	public genNames getGen()
	{
		if (iterator >= Cell.genomSize)
			iterator = 0;
		return list.get(iterator++);
	}
	
}


