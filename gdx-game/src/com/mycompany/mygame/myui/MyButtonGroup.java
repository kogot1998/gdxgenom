package com.mycompany.mygame.myui;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import java.util.*;

public class MyButtonGroup extends ButtonGroup
{
	private int left = 0, width = 0, top = 0, height = 0;
	private ArrayList<MyButtonGroup> BGlist;
	private boolean enabled = true;
	public MyButtonGroup(int x, int y, int width, int height)
	{
		left = x;
		top = y;
		this.width = width;
		this.height = height;
	}
	
	public MyButtonGroup()
	{
		left = 0;
		top = 0;
		width = 0;
		height = 0;
		BGlist = new ArrayList<MyButtonGroup>();
	}
	
	public int getX()
	{
		return left;
	}
	
	public int getY()
	{
		return top;
	}
	public int getWidth()
	{
		return width;
	}
	
	public int getHeight()
	{
		return height;
	}
	
	public void addButton(TextButton b)
	{
		if (getButtons().size == 0)
		{
			left = (int)b.getX();
			top = (int)b.getY();
			width = (int)b.getWidth();
			height = (int)b.getHeight();
			add(b);
		}else
		{
			if (left > b.getX())
			{
				left = (int)b.getX();
			}
			if (left + width < b.getX() + b.getWidth())
			{
				width = (int)(b.getX() + b.getWidth() - left);
			}
			if (top > b.getY())
			{
				top = (int)b.getY();
			}
			if (top + height < b.getY() + b.getHeight())
			{
				height = (int)(b.getY() + b.getHeight() - top);
			}
			add(b);
		}
	}
	public void addButtonGroup(MyButtonGroup b)
	{
		BGlist.add(b);
	}
	public ArrayList<MyButtonGroup> getButtonGroups()
	{
		return BGlist;
	}
	public boolean isEnabled()
	{
		return enabled;
	}
	
	public void setEnabled(boolean flag)
	{
		enabled = flag;
		if (flag == false)
		{
			for (MyButtonGroup b:BGlist)
			{
				b.setEnabled(false);
			}
		}
	}
}
