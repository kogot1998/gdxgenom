package com.mycompany.mygame;
import com.badlogic.gdx.graphics.*;
import java.util.*;

public class Cell extends Pane
{
	static int genomSize = 15, maxChild = 2, childChince = 5, suicideChince = 10,
	energyToClone = 40, energyPerWait = 1, energyFromDead = 5, energyToMove = 2,
	defaultAttack = 2, attackCoefficient = 3;
	public Gen gens;
	
	static int fullEnergy = 10;
	
	int energy, maxAttack;
	
	GenomFunctions func = new GenomFunctions();
	
	public Cell(int x, int y)
	{
		gens = new Gen();
		this.x = x;
		this.y = y;
		color = new Color();
		color = Color.GREEN;
		maxAttack = defaultAttack;
		energy = fullEnergy;
		type = 2;
		team = 0;
	}
	
	public Cell()
	{
		gens = new Gen();
		color = new Color();
		color = Color.GREEN;
		maxAttack = defaultAttack;
		energy = fullEnergy;
		type = 2;
		team = 0;
	}
	public void copy(Cell c,int xp,int yp)
	{
		gens.copy(c.gens);
		type = c.type;
		team = c.team;
		maxAttack = c.maxAttack + Gen.r.nextInt(3)-1;
		if (maxAttack <= 0)
			maxAttack = 1;
		energy = fullEnergy;
		color = c.color;
		x = xp;
		y = yp;
	}
	public Cell(Cell c)
	{
		type = c.type;
		maxAttack = c.maxAttack;
		color = new Color();
		color = c.color;
		energy = c.energy;
		gens = new Gen(c.gens);
		team = c.team;
	}
	
	public Cell(Cell c, boolean flag)//копирование при клонировании
	{
		type = c.type;
		maxAttack = c.maxAttack + Gen.r.nextInt(3)-1;
		if (maxAttack <= 0)
			maxAttack = 1;
		color = new Color();
		color = c.color;
		energy = fullEnergy;
		gens = new Gen(c.gens,flag);
		team = c.team;
	}
	
	public void step(Pane[][] field, ArrayList<Cell> list, ArrayList<Cell> newCells)
	{
		func.step(field, this, gens.getGen(), list, newCells);
	}
	
	public void Die()
	{
		energy = energyFromDead;
		type = 3;
		color = Color.GRAY;
	}
}
