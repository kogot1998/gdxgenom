package com.mycompany.mygame;
import com.badlogic.gdx.graphics.*;

public class Pane
{
	public Color color;
	public int x,y;
	public int type;//0 - пусто, 1- стена, 2 - клетка
	public int team;
	public Pane()
	{
		color = new Color();
		color = Color.WHITE;
		x = 0;
		y = 0;
		type = 0;
		team = -1;
	}
	
	public Pane(int x, int y)
	{
		color = new Color();
		color = Color.WHITE;
		this.x = x;
		this.y = y;
		type = 0;
		team = -1;
	}
	
	public Color getColor()
	{
		if (type == 2)
		{
			if (team == 0) 
				return Color.RED;
			if (team == 1) 
				return Color.GREEN;
			if (team == 2) 
				return Color.BLUE;	
			if (team == 3) 
				return Color.YELLOW;
			if (team == 4) 
				return Color.PINK;
			if (team == 5) 
				return Color.MAROON;;
		}
		return color;
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
}
