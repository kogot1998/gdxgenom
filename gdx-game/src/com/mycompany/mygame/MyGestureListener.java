package com.mycompany.mygame;
import com.mycompany.mygame.myui.*;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector;
import android.gesture.*;
import android.view.*;
import com.badlogic.gdx.math.*;
import com.badlogic.*;
import android.view.LayoutInflater.*;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.*;
import com.badlogic.gdx.graphics.glutils.*;
import java.util.ArrayList;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.*;

public class MyGestureListener implements GestureDetector.GestureListener
{
	private final float maxk=2,mink = 0.02f;
	//public float x0 = 0, y0 = 0, k = 1;
	public float x,y;//точка приближения
	public boolean longt = false;
	public int boomMode = 0;
	ArrayList<MyButtonGroup> glist;
	MyGdxGame game;
	
	public MyGestureListener(MyGdxGame g)
	{
		game = g;
		glist = new ArrayList<MyButtonGroup>();
	}
	
	@Override
	public boolean touchDown(float p1, float p2, int p3, int p4)
	{
	//	x = p1;
		//y = p2;
		return false;
	}

	private boolean checkTapInButtonGroup(MyButtonGroup b, float xp, float yp)
	{
		
		if (xp>b.getX() && xp < b.getX()+b.getWidth()&& yp> b.getY() && yp < b.getY()+b.getHeight())
		{//если одна из областей подошла
			for (TextButton t:b.getButtons())
			{
				if (xp>t.getX() && xp < t.getX()+t.getWidth()&& yp> t.getY() && yp < t.getY()+t.getHeight())
				{
					//t.hit(x,y,true);
					for (EventListener l : t.getListeners())
					{
						l.handle(new Event());
						//longt = true;
					}
					return true;

				}
			}
		}
		boolean res;
		for (MyButtonGroup g:b.getButtonGroups())
		{
			if (g.isEnabled())
			{
				res = checkTapInButtonGroup(g,xp,yp);
				if (res == true)
					return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean tap(float xp, float yp, int count, int button)
	{
		// TODO: Implement this method
		//longt = true;
		yp = Gdx.graphics.getHeight()-yp;
		
		if (boomMode != 0)
		{
			x = xp/game.k-game.x0/game.k;
			y = yp/game.k-game.y0/game.k;
			game.genom.boom((xp-game.x0)/game.k,(yp-game.y0)/game.k, boomMode);
			boomMode = 0;
			return false;
		}
		
		boolean res;
		for (MyButtonGroup b : glist)
		{
			if (!b.isEnabled())
				continue;
			res = checkTapInButtonGroup(b,xp,yp);
			if (res == true)
				return false;
			
		}
		x = xp/game.k-game.x0/game.k;
		y = yp/game.k-game.y0/game.k;
		return false;
	}

	
	
	@Override
	public boolean longPress(float p1, float p2)
	{
		game.k = 1;
		game.x0 = 0;
		game.y0 = 0;
		return false;
	}

	@Override
	public boolean fling(float p1, float p2, int p3)
	{
		// TODO: Implement this method
		return false;
	}

	boolean isPan = false;
	@Override
	public boolean pan(float p1, float p2, float p3, float p4)
	{
		if (isZoom == false && isPan == false)
		{
			isPan = true;
			game.x0 += p3;
			game.y0 -= p4;
			isPan = false;
		}
		return false;
	}

	@Override
	public boolean panStop(float p1, float p2, int p3, int p4)
	{
		// TODO: Implement this method
		return false;
	}

	@Override
	public boolean zoom(float p1, float p2)
	{
		return false;
	}

	
	private boolean isZoom = false;
	@Override
	public boolean pinch(Vector2 ifp, Vector2 isp, Vector2 fp, Vector2 sp)
	{
		if (isZoom == true && isPan == true)
			return false;
		isZoom = true;
		float xt =  (isp.x + ifp.x)/2;
		float yt = Gdx.graphics.getHeight() - (isp.y + ifp.y)/2;
		//float xt =x, yt = y;
		//x = xt;
	//	y = yt;
		float kt = (float)Math.sqrt((double)((fp.x-sp.x)*(fp.x-sp.x)+(fp.y-sp.y)*(fp.y-sp.y))/((ifp.x-isp.x)*(ifp.x-isp.x)+(ifp.y-isp.y)*(ifp.y-isp.y)));
		if (Math.abs(1 - kt)<0.05)
		{
			isZoom = false;
			return false;
		}
		if (kt< 1)//отдаленте
		{
			if (game.k < mink)
			{
				isZoom = false;
				return false;
			}
			kt = 0.999f-1/kt/100;
			//x = (xt - x0)/k;		
		//	y = (yt - y0)/k;
			game.k*=kt;
			game.x0 = xt - (xt-game.x0)*kt;
			game.y0 = yt - (yt-game.y0)*kt;
			
		}else
		if (kt > 1)//приближенте
		{
			if (game.k > maxk)
			{
				isZoom = false;
				return false;
			}
			kt = 1.001f+kt/100;
			//x = (xt - x0)/k;
			//y = (yt - y0)/k;
			game.k*=kt;
			game.x0 = xt - (xt-game.x0)*kt;
			game.y0 = yt - (yt-game.y0)*kt;
			
		}
		isZoom = false;
		return true;
		
	}

	public void printPoint(ShapeRenderer renderer)
	{
		renderer.begin(ShapeRenderer.ShapeType.Line);
		for (int i = (int)-MyGdxGame.lineWidth/2;i < MyGdxGame.lineWidth/2;i++)
		{
			renderer.line(0,game.y0 + y*game.k + i,Gdx.graphics.getWidth(),game.y0 + y*game.k + i);
			renderer.line(game.x0+x*game.k + i,0,game.x0+x*game.k + i,Gdx.graphics.getHeight());
		}
		renderer.end();
	}
}
