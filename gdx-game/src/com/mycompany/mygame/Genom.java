package com.mycompany.mygame;
import com.badlogic.gdx.graphics.glutils.*;
import org.apache.http.impl.cookie.*;
import java.util.*;
public class Genom
{
	
	static int size = 30, cellSize = 40, mSize = 0;
	
	Pane[][] field;
	ArrayList<Cell> list, newCells;
	
	public static final ArrayList<Cell> CellContainer = new ArrayList<Cell>();
	public static final ArrayList<Pane> PaneContainer = new ArrayList<Pane>();
	public static int containerSize = size*size;
	public Genom(int s)
	{
		size = s;
		list = new ArrayList<Cell>();
		newCells = new ArrayList<Cell>();
		field = new Pane[size][size];
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				field[i][j] = new Pane(i, j);
			}
		}
		
	//	for (int i = 0; i < parts; i++)
	//		t[i] = new MyThread(i);
		containerSize = s*s;
		CellContainer.clear();
		PaneContainer.clear();
		for(int i = 0; i<containerSize;i++)
		{
			CellContainer.add(new Cell());
			PaneContainer.add(new Pane());
		}
		
		for (int i = 0; i < threads; i++)
		{
			t[i] = new StepThread(this);
			t[i].start();
		}
			
		//fillContainer.start();
		addCell(1,1,1);
		addCell(25,25,2);
		addCell(1,25,4);
		addCell(25,1,3);
		addCell(25,10,5);
	}
	
	
	
	public  Cell newCell(Cell c, int x, int y)
	{
		Cell t;
		synchronized(CellContainer)
		{
			try{wait();}catch(Exception e){}
			t = CellContainer.get(0);
			CellContainer.remove(0);
		}
		t.copy(c,x,y);
		return t;
	}
	
	public  Pane newPane(int x, int y)
	{
		Pane t;
		synchronized(PaneContainer)
		{
			try{wait();}catch(Exception e){}
			t = PaneContainer.get(0);
			PaneContainer.remove(0);
		}
		t.x = x;
		t.y = y;
		return t;
	}
	
	/*public Genom()
	{
		list = new ArrayList<Cell>();
		field = new Pane[size][size];
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < size; j++)
			{
				field[i][j] = new Pane(i, j);
			}
		}
		
		//field[5][5] = new Wall(5,5);
		addCell(1,1,1);
		addCell(25,25,2);
		addCell(1,25,4);
		addCell(25,1,3);
		addCell(25,10,5);
	}*/
	
	public void addCell(int x, int y, int team)
	{
		Cell c = new Cell(x,y);
		field[x][y] = c;
		list.add(c);
		c.team = team;
	}
	//public static final int parts = 4;
	//MyThread t[] = new MyThread[parts];
	
	public void print(final ShapeRenderer renderer)
	{
		
		
		for (int i = 0; i < size; i++)
		{
			for (int j = 0; j < Genom.size; j++)
			{
				renderer.setColor(field[i][j].getColor());
				if (field[i][j].type == 2)
					renderer.rect(i*Genom.cellSize + Genom.mSize * i,j*Genom.cellSize + Genom.mSize * j,Genom.cellSize,Genom.cellSize);
				if (field[i][j].type == 3)
					renderer.circle(i*Genom.cellSize+Genom.mSize*i+Genom.cellSize/2,j*Genom.cellSize+Genom.mSize*j+Genom.cellSize/2,Genom.cellSize/2/Cell.energyFromDead*((Cell)field[i][j]).energy);
			}
		}
	}
	public void boom(float x0,float y0, int r)
	{
		int i = (int)(x0/(mSize + cellSize ));
		int j = (int)(y0/(mSize + cellSize ));
		for (int x = i - (r*2); x < i+(r*2);x++ )
		{
			if (x < 0 || x >= size )
				continue;
			for (int y = j - (r*2); y < j+(r*2);y++ )
			{
				if (y < 0 || y >= size )
					continue;
				int d = (int) Math.sqrt((x-i)*(x-i)+(y-j)*(y-j));
				if (d <= r)
				{
					if (field[x][y].type!=0)
					{
						CellContainer.add((Cell)field[x][y]);
						if (field[x][y].type==2)
							list.remove((Cell)field[x][y]);
						field[x][y] = newPane(x,y);
					}
				}
			}
		}
	}
	
	final int threads = 2;
	StepThread t[] = new StepThread[threads];
	
	public void Step()
	{
		newCells.clear();
		iterat = 0;
		for (int i = 0; i<threads;i++)
		{
			t[i].startSteps(field, list, newCells);
		}
	//	t[1].startSteps(field, list, newCells);
	//	t[2].startSteps(field, list, newCells);
		int y = 0;
	
		for (int i = 0; i<threads;i++)
		{
			if (t[i].isStart)
				i--;
		}
		//	y++;
		//	if(!t[0].isStart && !t[1].isStart && !t[2].isStart)
		//	{
		//		break;
		//	}
			//if (y >= 50000)
			//	break;
		
		list.addAll(newCells);
	//	int s = list.size();
		/*for(int i = 0; i < (list.size()); i++)
		{
			Cell p = list.get(i);
			p.step(field, list, newCells);
		}
		list.addAll(newCells);*/
	}
	
	static int iterat;
	
	public  void syncStep(Pane[][] field,ArrayList<Cell> list, ArrayList<Cell> newCells)
	{
		
		Cell p;
		synchronized(list)
		{	
			try {
			wait();
			}catch(Exception e)
			{
			}
			if (iterat >= list.size()-1)
			{
				t[0].stopSteps();
				t[1].stopSteps();
				t[2].stopSteps();
				list.addAll(newCells);
				return;
			}
			p = list.get(iterat);
			iterat++;
			/*if (iterat >= list.size())
			{
				t[0].stopSteps();
				t[1].stopSteps();
				t[2].stopSteps();
				list.addAll(newCells);
				return;
			}*/
			//notify();
		}	
		synchronized(p)
		{
			try
			{
				wait();
			}
			catch(Exception e)
			{}
			p.step(field, list, newCells);		
	//		notify();
		}
	}
}
