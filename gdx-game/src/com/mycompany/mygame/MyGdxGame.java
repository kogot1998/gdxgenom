package com.mycompany.mygame;

import com.mycompany.mygame.myui.*;
import com.badlogic.gdx.*;
import com.badlogic.gdx.InputProcessor.*;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.glutils.*;
import android.graphics.drawable.shapes.*;
import com.badlogic.gdx.scenes.scene2d.*;
import com.badlogic.gdx.utils.viewport.*;
import android.widget.*;
import android.content.*;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.input.*;
import com.badlogic.gdx.math.*;
import com.badlogic.gdx.assets.loaders.*;
import com.badlogic.gdx.scenes.scene2d.utils.*;
import java.util.ArrayList;
//import android.view.View.*;
//import android.view.*;

public class MyGdxGame implements ApplicationListener
{
	public static float lineWidth = 4;
	
	Stage stage;
	
	Texture texture;
	SpriteBatch batch;
	ShapeRenderer renderer;
	GestureDetector detector;
	MyGestureListener glistener;
	
	ArrayList<MyButtonGroup> bGroupList;
	
	MyButtonGroup BGroup, menuBgroup, sizeBgroup, cataclismBgroup, tempBgroup;
	Skin skin;
	public static float x0 = 0,y0 = 0;
	float k = 1;
	public static boolean start = false;
	Genom genom;
	
	@Override
	public void create()
	{
		bGroupList = new ArrayList<MyButtonGroup>();
		genom = new Genom(50);
		glistener = new MyGestureListener(this);
		
		detector = new GestureDetector(glistener);
		Gdx.input.setInputProcessor(detector);
		batch = new SpriteBatch();
		stage = new Stage(new ExtendViewport(Gdx.graphics.getWidth(),Gdx.graphics.getHeight()), batch);
		skin = new Skin(Gdx.files.internal("uiskin.json"));
		
		
		BGroup = new MyButtonGroup();
		menuBgroup = new MyButtonGroup();
	//	sizeBgroup = new MyButtonGroup(100,100,100,100);
		sizeBgroup = new MyButtonGroup();
		cataclismBgroup = new MyButtonGroup();
		tempBgroup = new MyButtonGroup();
		//tempBgroup.add(menuBgroup);
		BGroup.addButtonGroup(menuBgroup);
		menuBgroup.addButtonGroup(sizeBgroup);
		menuBgroup.addButtonGroup(cataclismBgroup);
		cataclismBgroup.addButtonGroup(tempBgroup);
		bGroupList.add(BGroup);
		//bGroupList.add(menuBgroup);
	///	bGroupList.add(sizeBgroup);
	//	bGroupList.add(cataclismBgroup);
		
		BgroupInitialise();
		MenuBgroupInitialise();
		sizeBgroupInitialise();
		cataclismBgroupInitialise();
		tempBgroupInitialise();
		for (MyButtonGroup g:bGroupList)
		{
			/*for (Button b:g.getButtons())
				stage.addActor(b);*/
			glistener.glist.add(g);
		}
		

		//for (Button b:sizeBgroup.getButtons())		stage.addActor(b);		
		texture = new Texture(Gdx.files.internal("android.jpg"));
		
		renderer = new ShapeRenderer();
	}

	public void BgroupInitialise()
	{
		TextButton menuButton = new TextButton("Menu",skin);
	
		menuButton.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e){
					if (menuBgroup.isEnabled())
					{
						menuBgroup.setEnabled(false);
						sizeBgroup.setEnabled(false);
						cataclismBgroup.setEnabled(false);
					}else
					menuBgroup.setEnabled(true);
					
					return false;
		}});
		
		menuButton.setBounds(0,0,100,50);
		BGroup.addButton(menuButton);

		TextButton next = new TextButton("next",skin);
		next.addListener(new EventListener()
			{	@Override
				public boolean handle(Event e){
					glistener.longt = true;
					return false;
		}});
		next.setBounds(100,0,100,50);
		BGroup.addButton(next);
		
		TextButton start = new TextButton("start",skin);
		start.addListener(new EventListener()
			{	@Override
				public boolean handle(Event e){
					
					//t.setPriority(2);
					//t.start();
					MyGdxGame.start = true;
					return false;
				}});
		start.setBounds(200,0,100,50);
		BGroup.addButton(start);
		
		TextButton stop = new TextButton("stop",skin);
		stop.addListener(new EventListener()
			{	@Override
				public boolean handle(Event e){
					MyGdxGame.start = false;
					//t.setPriority(1);
					return false;
				}});
		stop.setBounds(300,0,100,50);
		BGroup.addButton(stop);
	}
	/*Thread t =new Thread(){
		public void run()
		{
			while(true)
			{
				try
				{
					this.sleep(1000);
				}catch(Exception e)
				{}

				genom.Step();
				if (getPriority()==1)
					break;
			}
		}
	};*/
	public void MenuBgroupInitialise()
	{
		menuBgroup.setEnabled(false);
		
		TextButton resetB = new TextButton("reset", skin);
		resetB.setBounds(0,50,100,50);
		resetB.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					int t = genom.size;
					genom = new Genom(t);
					menuBgroup.setEnabled(false);
					return false;
		}});
		menuBgroup.addButton(resetB);
		
		TextButton resetCam = new TextButton("Cam.Reset", skin);
		resetCam.setBounds(0,100,100,50);
		resetCam.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					k = 1;
					x0 = 0;
					y0 = 0;
					menuBgroup.setEnabled(false);
					return false;
				}});
		menuBgroup.addButton(resetCam);
		
		TextButton sizeB = new TextButton("size", skin);
		sizeB.setBounds(0,150,100,50);
		sizeB.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					if (sizeBgroup.isEnabled())
						sizeBgroup.setEnabled(false);
					else
					{
						sizeBgroup.setEnabled(true);
						cataclismBgroup.setEnabled(false);
					}
					return false;
				}
			}
		);
		menuBgroup.addButton(sizeB);
		
		TextButton cataclism = new TextButton("cataclism", skin);
		cataclism.setBounds(0,200,100,50);
		cataclism.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					if (cataclismBgroup.isEnabled())
						cataclismBgroup.setEnabled(false);
					else
					{
						sizeBgroup.setEnabled(false);
						cataclismBgroup.setEnabled(true);
					}
					return false;
				}
			}
		);
		menuBgroup.addButton(cataclism);
	}
	
	public void sizeBgroupInitialise()//всплывающее меню смены размера
	{
		sizeBgroup.setEnabled(false);
		
		TextButton f = new TextButton("50x50",skin);
		f.setBounds(100,100,100,50);
		f.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					genom = new Genom(50);
					//genom.size = 50;
					sizeBgroup.setEnabled(false);
					menuBgroup.setEnabled(false);
					return false;
				}
			}
		);
		sizeBgroup.addButton(f);
		
		f = new TextButton("75x75",skin);
		f.setBounds(100,150,100,50);
		f.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					genom = new Genom(75);
					//genom.size = 75;
					sizeBgroup.setEnabled(false);
					menuBgroup.setEnabled(false);
					return false;
				}
			}
		);
		sizeBgroup.addButton(f);
		
		f = new TextButton("100x100",skin);
		f.setBounds(100,200,100,50);
		f.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					genom = new Genom(100);
					//genom.size = 75;
					sizeBgroup.setEnabled(false);
					menuBgroup.setEnabled(false);
					return false;
				}
			}
		);
		sizeBgroup.addButton(f);
		
		f = new TextButton("150x150",skin);
		f.setBounds(100,250,100,50);
		f.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					genom = new Genom(150);
					//genom.size = 75;
					sizeBgroup.setEnabled(false);
					menuBgroup.setEnabled(false);
					return false;
				}
			}
		);
		sizeBgroup.addButton(f);
		
		f = new TextButton("300x300",skin);
		f.setBounds(100,300,100,50);
		f.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					genom = new Genom(300);
					//genom.size = 75;
					sizeBgroup.setEnabled(false);
					menuBgroup.setEnabled(false);
					return false;
				}
			}
		);
		sizeBgroup.addButton(f);
	}
	
	void cataclismBgroupInitialise()
	{
		TextButton temp = new TextButton("temperature", skin);
		temp.setBounds(100,200,100,50);
		temp.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					if (tempBgroup.isEnabled())
						tempBgroup.setEnabled(false);
					else
						tempBgroup.setEnabled(true);
					return false;
				}
			}
		);
		cataclismBgroup.addButton(temp);
		
		TextButton sboom = new TextButton("small Boom", skin);
		sboom.setBounds(100,250,100,50);
		sboom.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					glistener.boomMode = 3;
					return false;
				}
			}
		);
		cataclismBgroup.addButton(sboom);
		
		TextButton mboom = new TextButton("medium Boom", skin);
		mboom.setBounds(100,300,100,50);
		mboom.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					glistener.boomMode = 6;
					return false;
				}
			}
		);
		cataclismBgroup.addButton(mboom);
		
		TextButton bboom = new TextButton("big Boom", skin);
		bboom.setBounds(100,350,100,50);
		bboom.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					glistener.boomMode = 9;
					return false;
				}
			}
		);
		cataclismBgroup.addButton(bboom);
		
		TextButton boom = new TextButton("Boom!!!", skin);
		boom.setBounds(100,400,100,50);
		boom.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					glistener.boomMode = 20;
					return false;
				}
			}
		);
		cataclismBgroup.addButton(boom);
		
	}
	void tempBgroupInitialise()
	{
		TextButton normal = new TextButton("normal", skin);
		normal.setBounds(200,200,100,50);
		normal.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					menuBgroup.setEnabled(false);
					Cell.energyPerWait = 3;
					return false;
				}
			}
		);
		tempBgroup.addButton(normal);
		
		TextButton cold = new TextButton("cold", skin);
		cold.setBounds(200,250,100,50);
		cold.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					menuBgroup.setEnabled(false);
					Cell.energyPerWait--;
					return false;
				}
			}
		);
		tempBgroup.addButton(cold);
		
		TextButton hot = new TextButton("hot", skin);
		hot.setBounds(200,300,100,50);
		hot.addListener(new EventListener()
			{
				@Override
				public boolean handle(Event e)
				{
					menuBgroup.setEnabled(false);
					Cell.energyPerWait++;
					return false;
				}
			}
		);
		tempBgroup.addButton(hot);
	}
	@Override
	public void render()
	{    
		//Thread.sleep(10);
		if (glistener.longt == true )
		{
			genom.Step();
			glistener.longt = false;
			
		}
		if (MyGdxGame.start == true)
		{
			genom.Step();
		}
		
		Gdx.gl.glClearColor(1, 1, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		
		//renderer.setProjectionMatrix(batch.getProjectionMatrix());
		//renderer.setTransformMatrix(batch.getTransformMatrix());
		renderer.setProjectionMatrix(stage.getBatch().getProjectionMatrix());
		renderer.setTransformMatrix(stage.getBatch().getTransformMatrix());
		
		//stage.act(Gdx.graphics.getDeltaTime());
		
		
		renderer.getTransformMatrix().translate(x0,y0,0);
		renderer.getTransformMatrix().scale(k,k,1);
			
		renderer.begin(ShapeRenderer.ShapeType.Filled);
		genom.print(renderer);
		renderer.setColor(Color.CYAN);
		renderer.end();
		
		renderer.setProjectionMatrix(stage.getBatch().getProjectionMatrix());
		renderer.setTransformMatrix(stage.getBatch().getTransformMatrix());
		
		glistener.printPoint(renderer);
		batch.begin();
		for (MyButtonGroup g:bGroupList)
		{
			printButtonsInBgroup(g, batch);
			
		}
		batch.end();
		
		renderer.getTransformMatrix().translate(x0,y0,0);
		renderer.getTransformMatrix().scale(k,k,1);
		renderer.begin(ShapeRenderer.ShapeType.Line);
		renderer.setColor(Color.GREEN);
		for (MyButtonGroup g:bGroupList)
		{
			printBgroup(g, renderer);
		/*	if (g.isEnabled())
			{
				renderer.rect(g.getX(), g.getY(), g.getWidth(), g.getHeight());
				renderer.rect(g.getX()+1, g.getY()+1, g.getWidth()-1, g.getHeight()-1);
			}*/
		}
		renderer.setColor(Color.CYAN);
		renderer.end();
		
	}

	void printButtonsInBgroup(MyButtonGroup g, Batch batch)
	{
		if (g.isEnabled())
			for (Button b:g.getButtons())
				b.draw(batch,1);
		for (MyButtonGroup b:g.getButtonGroups())
		{
			printButtonsInBgroup(b, batch);
		}
	}
	
	void printBgroup(MyButtonGroup g,ShapeRenderer renderer)
	{
		if (g.isEnabled())
		{
			renderer.rect(g.getX(), g.getY(), g.getWidth(), g.getHeight());
			renderer.rect(g.getX()+1, g.getY()+1, g.getWidth()-1, g.getHeight()-1);
		}
		for (MyButtonGroup b:g.getButtonGroups())
		{
			printBgroup(b,renderer);
		}
	}
	@Override
	public void dispose()
	{
		stage.dispose();
	}

	@Override
	public void resize(int width, int height)
	{
		stage.getViewport().update(width,height,true);
	}

	@Override
	public void pause()
	{
	}

	@Override
	public void resume()
	{
	}
}
