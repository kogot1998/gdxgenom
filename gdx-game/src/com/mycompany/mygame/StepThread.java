package com.mycompany.mygame;
import java.util.ArrayList;

public class StepThread extends Thread
{
	boolean isStart = false;
	Pane[][] field;
	ArrayList<Cell> list; 
	ArrayList<Cell> newCells;
	Genom genom;
	public StepThread(Genom g)
	{
		genom = g;
	}
	public void startSteps(Pane[][] field,ArrayList<Cell> list, ArrayList<Cell> newCells)
	{
		
		this.field = field;
		this.list = list;
		this.newCells=newCells;
		isStart = true;
	}
	
	public void stopSteps()
	{
		isStart = false;
	}
	
	@Override
	public void run()
	{
		while(true)
		{
			if (isStart)
			{
				Cell p;
				synchronized(list)
				{	
					try {
						wait(10);
					}catch(Exception e){}
					if (genom.iterat == -1)
					{
						isStart = false;
						//list.addAll(newCells);
						//newCells.clear();
						continue;
					}
					p = list.get(genom.iterat);
					genom.iterat++;
					if (genom.iterat >= list.size())
					{
						genom.iterat = -1;
						//list.addAll(newCells);
						//newCells.clear();
						isStart = false;
						continue;
					}
					
				}	
				synchronized(p)
				{
					try
					{
						wait();
					}
					catch(Exception e){}
					p.step(field, list, newCells);		
				}
		
			}else{
			try{
				sleep(30);
			}catch(Exception e){};}
		}
	}
}
