package com.mycompany.mygame;
import com.badlogic.gdx.graphics.*;

public class Wall extends Pane
{
	public Wall()
	{
		color = new Color();
		color = Color.BLACK;
		x = 0;
		y = 0;
		type = 1;
	}
	public Wall(int x, int y)
	{
		color = new Color();
		color = Color.BLACK;
		this.x = x;
		this.y = y;
		type = 1;
	}
}
